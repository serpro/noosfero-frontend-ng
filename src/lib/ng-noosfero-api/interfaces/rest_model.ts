namespace noosfero {
    export interface RestModel extends restangular.IElement {
        id: number;
    }
}