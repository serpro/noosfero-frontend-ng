import {Component, Input} from 'ng-forward';
import {BlockComponent} from '../block.component';

@Component({
    selector: 'noosfero-main-block',
    templateUrl: 'app/layout/blocks/main-block/main-block.html'
})
export class MainBlockComponent {

}
